using UnityEngine;

public class CueBallController : MonoBehaviour
{
    public float power;

    private void Start()
    {
        GetComponent<Rigidbody>().AddForce(power, 0, 0, ForceMode.Force);
    }
}
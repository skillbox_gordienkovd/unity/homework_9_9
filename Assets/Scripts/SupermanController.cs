using UnityEngine;

public class SupermanController : MonoBehaviour
{
    public float power;
    public float supermanPower;

    private void Start()
    {
        GetComponent<Rigidbody>().AddForce(supermanPower, 0, 0, ForceMode.Force);
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.rigidbody == null) return;

        var direction = other.transform.position - transform.position;
        other.rigidbody.AddForce(direction.normalized * power);
    }
}
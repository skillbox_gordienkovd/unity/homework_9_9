using UnityEngine;

public class ZeroGravitySphereController : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        GameObject.Find(other.name).GetComponent<Rigidbody>().useGravity = false;
    }

    private void OnTriggerExit(Collider other)
    {
        GameObject.Find(other.name).GetComponent<Rigidbody>().useGravity = true;
    }
}
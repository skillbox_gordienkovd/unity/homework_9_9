using UnityEngine;

public class BoomController : MonoBehaviour
{
    public float timeToExplosion;
    public float power;
    public float radius;
    public Rigidbody[] blocks;

    private void Start()
    {
        blocks ??= FindObjectsOfType<Rigidbody>();
    }

    private void Update()
    {
        timeToExplosion -= Time.deltaTime;

        if (timeToExplosion <= 0)
        {
           Boom();
        }
    }

    private void Boom()
    {
        foreach (var block in blocks)
        {
            var distance = Vector3.Distance(transform.position, block.transform.position);

            if (!(distance < radius)) continue;
            
            var direction = block.transform.position - transform.position;
            
            block.AddForce(direction.normalized * (power * (radius  - distance)), ForceMode.Impulse);
        }
        
        timeToExplosion = 3;
    }
}
